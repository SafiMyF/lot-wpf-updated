﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for AccessoriesView.xaml
    /// </summary>
    public partial class AccessoriesView : UserControl
    {
        public event EventHandler AccessoryCloseBtnClicked;
        public LotDBEntity.Models.BasicAccessories basicAccessory;
        public event EventHandler myEventCartCntUpdate;
        public event EventHandler myEventCartRefresh;
        public long ItemSNo = -1;
        public bool isFromCart = false;
        public AccessoriesView()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (basicAccessory != null)
                {
                    LogHelper.Logger.Info("When User Loaded started in AccessoriesView Page");
                    string stracc = basicAccessory.brand.Name.ToString();
                    stkAccessoryLogo.DataContext = Apps.lotContext.Brands.Where(c => c.Name == stracc).ToList();
                    this.DataContext = basicAccessory;
                    if (string.IsNullOrEmpty(basicAccessory.KeyFeature1.ToString()) && string.IsNullOrEmpty(basicAccessory.KeyFeature2) && string.IsNullOrEmpty(basicAccessory.KeyFeature3) && string.IsNullOrEmpty(basicAccessory.KeyFeature4))
                    {
                        KeyFeature.Visibility = Visibility.Collapsed;
                    }
                    if (basicAccessory.KeyFeature1 != "")
                    {
                        k1.Visibility = Visibility.Visible;
                    }
                    if (basicAccessory.KeyFeature2 != "")
                    {
                        k2.Visibility = Visibility.Visible;
                    }
                    if (basicAccessory.KeyFeature3 != "")
                    {
                        k3.Visibility = Visibility.Visible;
                    }
                    if (basicAccessory.KeyFeature4 != "")
                    {
                        k4.Visibility = Visibility.Visible;
                    }
                    if (isFromCart)
                    {
                        LotDBEntity.Models.BasicItem basicCartItem = StoreCompare_Cart.lstCart.Where(c => c.SNo == ItemSNo).FirstOrDefault();
                        LotDBEntity.Models.BasicAccessories basicCartAccessory = null;
                        if (basicCartItem.LstAccessories != null)
                            basicCartAccessory = basicCartItem.LstAccessories.Where(c => c.SNo == basicAccessory.SNo).FirstOrDefault();

                        if (basicCartAccessory != null)
                            btnCart.Content = "Remove";
                        else
                            isFromCart = false;
                    }
                    LogHelper.Logger.Info("When User Loaded Ended in AccessoriesView Page");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: AccessoriesViewPage : UserLoaded: " + ex.Message, ex);
            }
            
        }
        private void btnClose_TouchDown_1(object sender, TouchEventArgs e)
        {
            AccessoryCloseBtnClicked(this, null);
        }
        public event EventHandler AccessoryComment;
        private void btnAccessoryComment_TouchDown_1(object sender, TouchEventArgs e)
        {
            
        }
        private void Button_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("In AccessoryViewPage Button TouchDown Starts");
                if (!isFromCart)
                {
                    StoreCompare_Cart.AddCart<LotDBEntity.Models.BasicAccessories>(basicAccessory, sno: ItemSNo, isAccessory: true);
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                    this.Visibility = Visibility.Collapsed;
                }
                else
                {
                    LotDBEntity.Models.BasicItem baseItem = StoreCompare_Cart.lstCart.Where(c => c.SNo == ItemSNo).FirstOrDefault();
                    if (!string.IsNullOrEmpty(baseItem.ImageName))
                    {
                        if (baseItem.LstAccessories != null && baseItem.LstAccessories.Contains(basicAccessory))
                            baseItem.LstAccessories.Remove(basicAccessory);
                    }
                    else
                        StoreCompare_Cart.lstCart.Remove(baseItem);

                    StoreCompare_Cart.CartCnt -= 1;
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                    this.Visibility = Visibility.Collapsed;
                }
                LogHelper.Logger.Info("In AccessoryViewPage Button TouchDown Ends");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: AccessoriesViewPage : Button_TouchDown_1: " + ex.Message, ex);
            }
        }
    }
}
