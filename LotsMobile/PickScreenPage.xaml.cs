﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for PickScreenPage.xaml
    /// </summary>
    public partial class PickScreenPage : UserControl
    {
        public event EventHandler myEventBacktoAdminSetting;
        public PickScreenPage()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            LotDBEntity.Models.BasicItem basicItem = new LotDBEntity.Models.BasicItem();
            basicItem.ItemName = "SELECT ITEM";
            List<LotDBEntity.Models.BasicItem> lstBasicItem = Apps.lotContext.BasicItems.ToList();
            lstBasicItem.Insert(0, basicItem);
            CboItems.ItemsSource = lstBasicItem;
            CboItems.SelectedIndex = 0;
        }

        private void btnBacktoAdminSetting_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSetting(this, null);
        }
        private void btnCancel_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSetting(this, null);
        }

        private void btnSave_TouchDown_1(object sender, TouchEventArgs e)
        {
            long itemID = GetSelectedItemID();
            long totSeconds = GetSeconds();

            if (itemID > 0 && totSeconds>0)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                config.AppSettings.Settings["ItemID"].Value = Convert.ToString(itemID);
                config.AppSettings.Settings["ScreenSaverTimeSpan"].Value = Convert.ToString(totSeconds);
                config.Save();
                if (MessageBox.Show("Please restart your application to take effect your changes...\nPress ok to restart....", "LOT", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK)
                {
                    Utilities.launchAppImmediately = true;
                    Application.Current.Shutdown();
                }
                myEventBacktoAdminSetting(this, null);
            }
        }

        private long GetSeconds()
        {
            if (CboPrice.Text.Equals("30Sec"))
                return 30;
            else if (CboPrice.Text.Equals("1Min"))
                return 60;
            else if (CboPrice.Text.Equals("3Min"))
                return 180;
            else if (CboPrice.Text.Equals("5Min"))
                return 300;
            else
                return 60;
        }

        private long GetSelectedItemID()
        {
            LotDBEntity.Models.BasicItem selectedItem = CboItems.SelectedItem as LotDBEntity.Models.BasicItem;
            if (selectedItem != null && selectedItem.ItemName != "SELECT ITEM")
                return selectedItem.SNo;
            else
                MessageBox.Show("Please select Item to continue.", "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
            return 0;
        }
    }
}
