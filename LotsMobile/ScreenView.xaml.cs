﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for ScreenView.xaml
    /// </summary>
    public partial class ScreenView : Window
    {
        public LotDBEntity.Models.BasicItem BasicItem { get; set; }
        public ScreenView()
        {
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts UserControl_Loaded_1 in ItemViewPage");
                UserComments.ItemsSource = BasicItem.LstComments.ToList();
                this.DataContext = BasicItem;
                if (BasicItem.InternalStorage != "NOT AVAILABLE" || BasicItem.ExternalStorage != "NOT AVAILABLE")
                {
                    LabelMemory.Visibility = Visibility.Visible;
                }
                if (BasicItem.PrimaryCamera != "NOT AVAILABLE" || BasicItem.SecondaryCamera != "NOT AVAILABLE")
                {
                    LabelCamera.Visibility = Visibility.Visible;
                }
                if (string.IsNullOrEmpty(BasicItem.OSName))
                {
                    stkOS.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.ProcessorType))
                {
                    stkProcessor.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.DisplayType))
                {
                    stkDisplay.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.PrimaryCamera))
                {
                    stkCamera.Visibility = Visibility.Collapsed;
                }
                if (string.IsNullOrEmpty(BasicItem.PrimaryCamera) && string.IsNullOrEmpty(BasicItem.SecondaryCamera))
                {
                    stkMemory.Visibility = Visibility.Collapsed;
                }
                LogHelper.Logger.Info("Ends UserControl_Loaded_1 in ItemViewPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ItemViewPage : UserControl_Loaded_1: " + ex.Message, ex);
            }
        }

        private void Window_TouchDown_1(object sender, TouchEventArgs e)
        {
            this.Close();
        }
    }
}
