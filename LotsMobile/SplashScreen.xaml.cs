﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        BackgroundWorker bgw = null;
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            bgw = new BackgroundWorker();
            bgw.WorkerReportsProgress = true;
            bgw.WorkerSupportsCancellation = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                //string appPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                //if (myfashionsecurity.AuthenticationCodeLogic.CheckProductHasLicense(appPath, dbContext))
                {
                    //Write code here to load usercontrol or launch window
                    bgw = null;
                    System.Threading.Thread.Sleep(1000);
                    MainWindow HomePage = new MainWindow();
                    HomePage.DBContext = dbContext;
                    this.Hide();
                    HomePage.Show();
                    this.Close();
                }
            }
            catch (myfashionsecurity.SecurityException securityexc)
            {
                MessageBox.Show(securityexc.Message.ToString(), "myfashions", MessageBoxButton.OK, MessageBoxImage.Information);
                Application.Current.Shutdown();
            }
            catch (Exception ex) 
            {
                LogHelper.Logger.ErrorException(ex.Message, ex);
            }
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtText.Text = e.UserState as string;
        }

        LotDBEntity.DBFolder.LotDBContext dbContext = null;
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {

            bgw.ReportProgress(1, "Initializing Database... Please wait...");
            dbContext = new LotDBEntity.DBFolder.LotDBContext(Utilities.connectionString);
            bgw.ReportProgress(1, "Loading Database... Please wait...");
            var database = dbContext.BasicItems.ToList();
            var products = dbContext.BasicAccessories.ToList();
            Apps.lotContext = dbContext;
            bgw.ReportProgress(1, "Verifying License Information... Please wait...");
        }

        private void Window_Closing_1(object sender, CancelEventArgs e)
        {   
            if(bgw!=null)
            {
                bgw.CancelAsync();
                bgw = null;
            }
            this.Hide();
        }
    }
}
