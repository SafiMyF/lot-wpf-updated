﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// public long ItemSNo = -1;
    /// Interaction logic for Cart.xaml
    /// </summary>
    public partial class Cart : UserControl
    {
        public long ItemSNo = -1;
        public event EventHandler myEventCartCntUpdate;
        public event EventHandler myEventCartRefresh;
        public delegate void myEventCartItem(object sender, EventArgs e);
        public event myEventCartItem MyEventCartItemSelect;
        LotDBEntity.Models.BasicAccessories basicAccessory;
        bool isScrollMoved = false;
        Button btnTouchedItem = null;
        public double verticalOffset = 0;

        public Cart()
        {
            InitializeComponent();
        }

        private void btnClose_TouchDown_1(object sender, TouchEventArgs e)
        {
            sendEmailGrid.Visibility = Visibility.Collapsed;
        }

        private void btnSendEmail_TouchDown_1(object sender, TouchEventArgs e)
        {
            sendEmailGrid.Visibility = Visibility.Visible;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Cart Page UseControl Loaded Starts");
                double ScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
                grdCartMainPanel.Width = ScreenWidth;
                CartContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                CheckIsCartEmpty();

                // CartContext.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("SNo", System.ComponentModel.ListSortDirection.Descending));
                ScrollViewer.ScrollToVerticalOffset(verticalOffset);
                LogHelper.Logger.Info("Cart Page UseControl Loaded Ends");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CartPage : UserControl_Loaded_1: " + ex.Message, ex);
            }
            
        }

        private void CheckIsCartEmpty()
        {
            if (CartContext.Items.Count == 0)
            {
                imgEmptyCart.Visibility = Visibility.Visible;
                imgCartbg.Visibility = Visibility.Collapsed;
            }
            else
            {
                imgEmptyCart.Visibility = Visibility.Collapsed;
                imgCartbg.Visibility = Visibility.Visible;
            }
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnRemoveItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Started ButtonRemoveItem TouchDown in Cart Page");
                Button btnSelectedRemove = sender as Button;
                LotDBEntity.Models.BasicItem samp = btnSelectedRemove.DataContext as LotDBEntity.Models.BasicItem;
                if (samp != null)
                {
                    StoreCompare_Cart.lstCart.Remove(samp);
                    StoreCompare_Cart.CartCnt = StoreCompare_Cart.lstCart.Count;
                    CartContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                    CheckIsCartEmpty();
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                }
                else
                {
                    var selectedItem = FindAnchestor<StackPanel>(btnSelectedRemove, "stkCartAccessory");
                    LotDBEntity.Models.BasicItem sample = selectedItem.DataContext as LotDBEntity.Models.BasicItem;
                    StoreCompare_Cart.lstCart.Remove(sample);
                    StoreCompare_Cart.CartCnt = StoreCompare_Cart.lstCart.Count;
                    CartContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                    CheckIsCartEmpty();
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                }
                LogHelper.Logger.Info("Ended ButtonRemoveItem TouchDown in Cart Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CartPage : btnRemoveItem_TouchDown_1: " + ex.Message, ex);
            }
            
        }

        private static T FindAnchestor<T>(DependencyObject current, string itemName) where T : DependencyObject
        {
            do
            {
                if (current is T && ((FrameworkElement)current).Name == itemName)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);

            return null;
        }

        private void btnCartItemSelect_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btnCartItem = sender as Button;
            btnTouchedItem = btnCartItem;
            if (!isScrollMoved)
            {
                //myEventListingItem(btn, null);
            }
            else
                isScrollMoved = false;
        }

        private void btnRemoveItemAccesory_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Started ButtonRemoveItemAccessory TouchDown in CartPage");
                Button btnSelectedRemove = sender as Button;
                var selectedItem = FindAnchestor<StackPanel>(btnSelectedRemove, "cartListingStkpnl");
                basicAccessory = btnSelectedRemove.DataContext as LotDBEntity.Models.BasicAccessories;

                LotDBEntity.Models.BasicItem baseItem = selectedItem.DataContext as LotDBEntity.Models.BasicItem;
                if (baseItem != null && !string.IsNullOrEmpty(baseItem.ImageName))
                {
                    if (baseItem.LstAccessories != null && baseItem.LstAccessories.Contains(basicAccessory))
                        baseItem.LstAccessories.Remove(basicAccessory);
                    CartContext.ItemsSource = StoreCompare_Cart.lstCart.ToList();
                    CheckIsCartEmpty();
                    StoreCompare_Cart.CartCnt -= 1;
                    myEventCartCntUpdate(this, null);
                    myEventCartRefresh(this, null);
                }
                LogHelper.Logger.Info("Ended ButtonRemoveItemAccessory TouchDown in CartPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: CartPage : btnRemoveItemAccesory_TouchDown_1: " + ex.Message, ex);
            }
           
        }
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        private void ScrollViewer_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void ScrollViewer_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void ScrollViewer_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    MyEventCartItemSelect(btnTouchedItem, null);
                }
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void ScrollViewer_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        private void btnPlus_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btnCartItem = sender as Button;
            btnTouchedItem = btnCartItem;
        }

    }
}
