﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq.Dynamic;
using LotsMobile.Models;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for UserOptions.xaml
    /// </summary>
    public partial class UserOptions : UserControl
    {
        public event EventHandler myEventUserOption;
        public event EventHandler myEventAccListing;
        public event EventHandler myEventMoreOffer;
        public string mainExpression = string.Empty;
        public LotDBEntity.Models.Brands selectedItemBrand = null;
        public UserOptions()
        {
            InitializeComponent();
        }

        string priceExpression;
        private void btnSearch_TouchDown_1(object sender, TouchEventArgs e)
        {
            priceExpression = string.Empty;
            List<LotDBEntity.Models.BasicItem> lstChoosedItems = new List<LotDBEntity.Models.BasicItem>();
            mainExpression = string.Empty;
            if (CboPrice.Visibility == Visibility.Visible && CboPrice.SelectedIndex > 0)
            {
                if (!string.IsNullOrEmpty(mainExpression))
                {
                    mainExpression += " and ";
                }

                priceExpression += retPriceExpr();
                mainExpression += priceExpression;
            }

            if (CboBrand.Visibility == Visibility.Visible)
            {
                if (CboBrand != null && CboBrand.SelectedItem != null && CboBrand.SelectedIndex > 0)
                {
                    LotDBEntity.Models.Brands brand = CboBrand.SelectedItem as LotDBEntity.Models.Brands;

                    if (!string.IsNullOrEmpty(mainExpression))
                    {
                        mainExpression += " and ";
                    }

                    mainExpression += "ItemBrand.SNo=" + brand.SNo;
                }
            }

            if (!string.IsNullOrEmpty(mainExpression))
            {
                lstChoosedItems = Apps.lotContext.BasicItems.AsQueryable().Where(mainExpression).ToList();
                UserOptionModels userOptionModel = new UserOptionModels();
                userOptionModel.lstChoosedItems = lstChoosedItems;
                userOptionModel.selectedPriceIndex = CboPrice.SelectedIndex;
                userOptionModel.SelectedItemBrand = CboBrand.SelectedItem as LotDBEntity.Models.Brands;
                myEventUserOption(userOptionModel, null);
            }
            else
            {
                myEventUserOption(this, null);
            }

        }

        private void btnFindUrOffer_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventMoreOffer(this, null);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
            brand.Name = "SELECT BRAND";
            List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.BasicItems.GroupBy(c => c.ItemBrand).Select(d => d.Key).ToList();
            lstBrands.Insert(0, brand);
            CboBrand.ItemsSource = lstBrands;
            CboBrand.SelectedIndex = 0;
        }

        private string retPriceExpr()
        {
            priceExpression = string.Empty;
            if (CboPrice.SelectedIndex == 1)
                priceExpression += "DefaultPrice<5000";
            else if (CboPrice.SelectedIndex == 2)
                priceExpression += "DefaultPrice>=5000 and DefaultPrice<=10000";
            else if (CboPrice.SelectedIndex == 3)
                priceExpression += "DefaultPrice>10000 and DefaultPrice<=15000";
            else if (CboPrice.SelectedIndex == 4)
                priceExpression += "DefaultPrice>15000 and DefaultPrice<=25000";
            else if (CboPrice.SelectedIndex == 5)
                priceExpression += "DefaultPrice>25000 and DefaultPrice<=35000";
            else if (CboPrice.SelectedIndex == 6)
                priceExpression += "DefaultPrice>35000 and DefaultPrice<=45000";
            else if (CboPrice.SelectedIndex == 7)
                priceExpression += "DefaultPrice>45000 and DefaultPrice<=55000";
            else if (CboPrice.SelectedIndex == 8)
                priceExpression += "DefaultPrice>55000";

            //mainExpression += priceExpression;
            return priceExpression;
        }

        private void CboPrice_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            priceExpression = retPriceExpr();

            if (!string.IsNullOrEmpty(priceExpression))
            {
                List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.AsQueryable().Where(priceExpression).ToList();
                if (lstBasicItems != null && lstBasicItems.Count > 0)
                {
                    LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                    brand.Name = "SELECT BRAND";

                    List<LotDBEntity.Models.Brands> lstFilteredBrands = lstBasicItems.GroupBy(c => c.ItemBrand).Select(c => c.Key).ToList();
                    lstFilteredBrands.Insert(0, brand);

                    CboBrand.ItemsSource = lstFilteredBrands;
                    CboBrand.SelectedIndex = 0;
                }
                else
                {
                    LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                    brand.Name = "SELECT BRAND";
                    List<LotDBEntity.Models.Brands> lstBrands = new List<LotDBEntity.Models.Brands>();
                    lstBrands.Insert(0, brand);
                    CboBrand.ItemsSource = lstBrands;
                    CboBrand.SelectedIndex = 0;
                }
            }
            else
            {
                LotDBEntity.Models.Brands brand = new LotDBEntity.Models.Brands();
                brand.Name = "SELECT BRAND";
                List<LotDBEntity.Models.Brands> lstBrands = Apps.lotContext.BasicItems.GroupBy(c => c.ItemBrand).Select(c => c.Key).ToList();
                lstBrands.Insert(0, brand);
                if (CboBrand != null)
                {
                    CboBrand.ItemsSource = lstBrands;
                    CboBrand.SelectedIndex = 0;
                }
            }
        }

        private void CboBrand_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnSearchAcc_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventAccListing(this, null);
        }
    }
}
