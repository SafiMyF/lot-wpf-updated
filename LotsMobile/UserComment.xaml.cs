﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for UserComment.xaml
    /// </summary>
    public partial class UserComment : Window
    {
        public LotDBEntity.Models.BasicItem basicItem;
        public event EventHandler refreshComments;
        public UserComment()
        {
            InitializeComponent();
            txtUserName.Focus();
        }

        private void btnCommentSave_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("When Button Save Comments Touches Started");
                if (!string.IsNullOrWhiteSpace(txtUserName.Text) && !string.IsNullOrWhiteSpace(txtUserComment.Text))
                {
                    if (basicItem.LstComments == null)
                    {
                        LotDBEntity.Models.Comments comment = new LotDBEntity.Models.Comments();
                        comment.CustomerName = txtUserName.Text;
                        comment.CustomerMobile = txtUserContactNo.Text;
                        comment.Comment = txtUserComment.Text;
                        comment.baseItem = basicItem;

                        List<LotDBEntity.Models.Comments> lstComments = new List<LotDBEntity.Models.Comments>();
                        lstComments.Add(comment);
                        basicItem.LstComments = lstComments;
                        Apps.lotContext.SaveChanges();
                    }
                    else
                    {
                        LotDBEntity.Models.Comments comment = new LotDBEntity.Models.Comments();
                        comment.CustomerName = txtUserName.Text;
                        comment.CustomerMobile = txtUserContactNo.Text;
                        comment.Comment = txtUserComment.Text;
                        comment.baseItem = basicItem;
                        comment.ID = 1;
                        comment.LastUpdatedTime = DateTime.Now;

                        //basicItem.LstComments.Add(comment);
                        basicItem.LstComments.Insert(0, comment);
                        Apps.lotContext.SaveChanges();
                    }
                    LogHelper.Logger.Info("When Button Save Comments Touches Ended");
                    UserCommentPanel.Visibility = Visibility.Collapsed;
                    txtUserName.Text = "";
                    txtUserContactNo.Text = "";
                    txtUserComment.Text = "";
                    refreshComments(this, null);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("* Please enter required fields.");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : btnCommentSave_TouchDown_1: " + ex.Message, ex);
            }
        }

        private void btnCloseComment_TouchDown_1(object sender, TouchEventArgs e)
        {
            this.Close();
        }

        private void txtUserContactNo_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
            {
                e.Handled = true;

            }
        }
    }
}
