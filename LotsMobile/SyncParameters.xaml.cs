﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for SyncParameters.xaml
    /// </summary>
    public partial class SyncParameters : UserControl
    {
        public SyncParameters()
        {
            InitializeComponent();
        }

        public event EventHandler myEventBacktoAdminSettings;
        private void btnBacktoAdminSetting_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSettings(this, null);
        }

        private void btnSave_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIP.Text.Trim()) && !string.IsNullOrEmpty(txtPortNum.Text.Trim()))
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["AutoSyncIP"].Value = txtIP.Text.Trim();
                config.AppSettings.Settings["AutoSyncPort"].Value = txtPortNum.Text.Trim();
                config.Save();

                string connectionString = config.ConnectionStrings.ConnectionStrings["SqlServerMainDBConnectString"].ConnectionString;

                string[] splittedString = connectionString.Split(';');
                string modifiedString = string.Empty;
                foreach (string item in splittedString)
                {
                    if (item.Contains("Data Source"))
                        modifiedString += "Data Source=" + txtIP.Text.Trim() + ";";
                    else if (!item.Equals(splittedString[splittedString.Length - 1], StringComparison.CurrentCultureIgnoreCase))
                        modifiedString += item + ";";
                    else
                        modifiedString += item;
                }
                config.ConnectionStrings.ConnectionStrings["SqlServerMainDBConnectString"].ConnectionString = modifiedString;
                config.Save();

                if (MessageBox.Show("Please restart your application to take effect your changes...\nPress ok to restart....", "LOT", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK)
                {
                    Utilities.launchAppImmediately = true;
                    Application.Current.Shutdown();
                }

                myEventBacktoAdminSettings(this, null);
            }
        }

        private void btnClear_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            txtIP.Text = string.Empty;
            txtPortNum.Text = string.Empty;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            txtIP.Text = config.AppSettings.Settings["AutoSyncIP"].Value;
            txtPortNum.Text = config.AppSettings.Settings["AutoSyncPort"].Value;
        }
    }
}
