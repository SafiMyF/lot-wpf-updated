﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for UserOffers.xaml
    /// </summary>
    public partial class UserOffers : UserControl
    {
        public event EventHandler myEventFindUrMatch;
        public event EventHandler myEventSelectedMatch;

        public UserOffers()
        {
            InitializeComponent();
        }

        private void btnFindUrMatch_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventFindUrMatch(this, null);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {   
            grdOfferImage.DataContext = Apps.lotContext.OffersOfTheDay.ToList();
        }

        private void btnEMI_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.EMIAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);            
        }

        private void btnExchange_TouchDown_1(object sender, TouchEventArgs e)
        {
            //List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.ExchangeAvailable == true).ToList();
            //StoreCompare_Cart.IsFromOfferPage = 1;
            //myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnFinance_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.FinancingOptionsAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnOffer_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.OffersAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnTodaysOffer_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.ExchangeAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnTrail_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.TrailPeriodAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnPower_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.PowerBankFreeAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnFlat_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.Flat50BuyBackAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnBuy1Get2_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.BuyOneGetTwoAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }

        private void btnBuy1Get1_TouchDown_1(object sender, TouchEventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = Apps.lotContext.BasicItems.Where(c => c.BuyOneGetOneAvailable == true).ToList();
            StoreCompare_Cart.IsFromOfferPage = 1;
            myEventSelectedMatch(lstBasicItems, null);
        }
    }
}
