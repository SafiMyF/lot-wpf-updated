﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for AdminSettings.xaml
    /// </summary>
    public partial class AdminSettings : UserControl
    {
        //public event EventHandler myEventBackSettings;
        public event EventHandler myEventLogoutSettings;
        public event EventHandler myEventAppDetails;
        public event EventHandler myEventAppSync;

        public AdminSettings()
        {
            InitializeComponent();
        }

        private void btnLogout_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventLogoutSettings(this, null);
        }

        public event EventHandler myEventSyncParameters;
        private void btnSyncParamenter_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventSyncParameters(this, null);
        }

        private void btnShowDesktop_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer");
        }

        private void btnAppdetails_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventAppDetails(this, null);
        }

        private void btnAppAutoSync_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            myEventAppSync(this, null);
        }

        private void btnShutdown_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (MessageBox.Show("Are you sure want to shutdown the system.", "LOT", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");
            }
        }

        private void btnRestart_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (MessageBox.Show("Are you sure want to restart the system.", "LOT", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /f /t 0");
            }
        }
    }
}
