﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace LotsMobile
{
    public class InactivityTimer : IDisposable
    {
        public event EventHandler Inactivity;

        public TimeSpan TimeOut { get; private set; }

        public TimeSpan InactivityTime
        {
            get
            {
                return TimeSpan.FromMilliseconds(Environment.TickCount - _lastActivityTime);
            }
        }

        int _lastActivityTime;
        DispatcherTimer _timer;

        public InactivityTimer(TimeSpan timeOut)
        {
            if (timeOut <= TimeSpan.Zero || timeOut.TotalMilliseconds > int.MaxValue)
            {
                throw new ArgumentOutOfRangeException("timeOut", timeOut, string.Format("Must be a positive number less than {0}.", int.MaxValue));
            }

            TimeOut = timeOut;

            _lastActivityTime = Environment.TickCount;

            InputManager.Current.PreNotifyInput += new NotifyInputEventHandler(Current_PreNotifyInput);

            _timer = new DispatcherTimer()
            {
                Interval = timeOut,
            };

            _timer.Tick += new EventHandler(_timer_Tick);

            _timer.Start();
        }

        void Current_PreNotifyInput(object sender, NotifyInputEventArgs e)
        {
            if (e.StagingItem.Input is MouseEventArgs
                || e.StagingItem.Input is KeyboardEventArgs
                || e.StagingItem.Input is TextCompositionEventArgs
                || e.StagingItem.Input is TouchEventArgs
                || e.StagingItem.Input is StylusEventArgs)
            {
                _lastActivityTime = Environment.TickCount;
            }
            else
            {
                
            }
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            var time = Environment.TickCount;
            var inactivityTime = _lastActivityTime + (int)TimeOut.TotalMilliseconds;
            if (inactivityTime - time <= 0)
            {
                var temp = Inactivity;
                if (temp != null)
                {
                    temp(this, EventArgs.Empty);
                }
                _lastActivityTime = time;
                _timer.Interval = TimeOut;
            }
            else
            {
                _timer.Interval = TimeSpan.FromMilliseconds(inactivityTime - time);
            }
        }

        public void Reset()
        {
            _timer.Dispatcher.VerifyAccess();
            _lastActivityTime = Environment.TickCount;
        }

        public void Dispose()
        {
            _timer.Dispatcher.VerifyAccess();
            _timer.Tick -= new EventHandler(_timer_Tick);
            _timer.Stop();
            InputManager.Current.PreNotifyInput -= new NotifyInputEventHandler(Current_PreNotifyInput);
        }
    }
}
