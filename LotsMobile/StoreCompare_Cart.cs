﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotsMobile
{
    public class StoreCompare_Cart
    {
        public static ObservableCollection<LotDBEntity.Models.BasicItem> lstCompare = new ObservableCollection<LotDBEntity.Models.BasicItem>();
        public static ObservableCollection<LotDBEntity.Models.BasicItem> lstCart = new ObservableCollection<LotDBEntity.Models.BasicItem>();
        public static int CartCnt = 0;
        public static int CmpreCnt = 0;

        public static int IsFromOfferPage = 0;

        public static void AddCart<T>(T choosenObject, long sno = -1, bool isAccessory = false)
        {
            if (isAccessory)
            {
                LotDBEntity.Models.BasicAccessories basicAccessory = new LotDBEntity.Models.BasicAccessories();
                LotDBEntity.Models.BasicItem basicItem = lstCart.Where(c => c.SNo == sno).FirstOrDefault();
                if (choosenObject is LotDBEntity.Models.BasicAccessories)
                    basicAccessory = choosenObject as LotDBEntity.Models.BasicAccessories;
                if (basicItem != null && !string.IsNullOrEmpty(basicItem.ImageLocation))
                {
                    LotDBEntity.Models.BasicAccessories checkAccessory = null;
                    if (basicItem.LstAccessories != null)
                        checkAccessory = basicItem.LstAccessories.Where(c => c.SNo == basicAccessory.SNo).FirstOrDefault();
                    if (checkAccessory == null && basicItem.LstAccessories != null)
                    {
                        if (!basicItem.LstAccessories.Contains(basicAccessory))
                        {
                            basicItem.LstAccessories.Add(basicAccessory);
                            CartCnt += 1;
                        }
                    }
                    else if (checkAccessory == null)
                    {
                        List<LotDBEntity.Models.BasicAccessories> lstBasicAccessory = new List<LotDBEntity.Models.BasicAccessories>();
                        lstBasicAccessory.Add(basicAccessory);
                        basicItem.LstAccessories = lstBasicAccessory;
                        CartCnt  += 1;
                    }
                }
                LotDBEntity.Models.BasicItem checkBaseItem = lstCart.Where(c => c.SNo == sno && c.LstAccessories.ToList().Exists(d => d.SNo == basicAccessory.SNo)).FirstOrDefault();
                if (checkBaseItem == null)
                {
                    LotDBEntity.Models.BasicItem baseItem = new LotDBEntity.Models.BasicItem();
                    baseItem.SNo = sno;
                    List<LotDBEntity.Models.BasicAccessories> lstBasicAccessory = new List<LotDBEntity.Models.BasicAccessories>();
                    lstBasicAccessory.Add(basicAccessory);
                    baseItem.LstAccessories = lstBasicAccessory;
                    //lstCart.Add(baseItem);
                    lstCart.Insert(0, baseItem);
                    CartCnt += 1;
                }
                
            }
            else
            {
                List<LotDBEntity.Models.BasicItem> insertedItem = lstCart.ToList().Where(c => c.SNo == sno).ToList();
                LotDBEntity.Models.BasicItem insertSingleItem = new LotDBEntity.Models.BasicItem();
                LotDBEntity.Models.BasicItem basicItem = new LotDBEntity.Models.BasicItem();
                if (insertedItem == null || insertedItem.Count == 0)
                {
                    //lstCart.Add(insertSingleItem);
                    lstCart.Insert(0, insertSingleItem);
                    CartCnt += 1;
                }
                if (choosenObject is LotDBEntity.Models.BasicItem)
                    basicItem = choosenObject as LotDBEntity.Models.BasicItem;
                if (insertedItem == null || insertedItem.Count == 0)
                    insertSingleItem = CopytoInsertedItem(basicItem, insertSingleItem);
                else
                {
                    insertSingleItem = CopytoInsertedItem(basicItem, insertedItem);
                    for (int i = 0; i < insertedItem.Count; i++)
                    {
                        if (i > 0)
                        {
                            LotDBEntity.Models.BasicItem bItem = insertedItem[i] as LotDBEntity.Models.BasicItem;
                            lstCart.Remove(bItem);

                            CartCnt -= (1 + bItem.LstAccessories.Count);
                        }
                    }
                    if (insertedItem.Contains(basicItem))
                    {
                        CartCnt += 1;
                    }
                   
                }
            }
        }

        private static LotDBEntity.Models.BasicItem CopytoInsertedItem(LotDBEntity.Models.BasicItem baseItem, List<LotDBEntity.Models.BasicItem> insertedItems)
        {
            insertedItems[0].SNo = baseItem.SNo;
            insertedItems[0].ItemName = baseItem.ItemName;
            insertedItems[0].ItemCode = baseItem.ItemCode;

            insertedItems[0].Length = baseItem.Length;
            insertedItems[0].Breadth = baseItem.Breadth;
            insertedItems[0].Height = baseItem.Height;

            insertedItems[0].ImageName = baseItem.ImageName;
            insertedItems[0].ImageLocation = baseItem.ImageLocation;
            insertedItems[0].DefaultPrice = baseItem.DefaultPrice;
            insertedItems[0].ItemColor = baseItem.ItemColor;
            insertedItems[0].Description = baseItem.Description;
            insertedItems[0].ProcessorType = baseItem.ProcessorType;
            insertedItems[0].ProcessorSpeed = baseItem.ProcessorSpeed;
            insertedItems[0].DisplayType = baseItem.DisplayType;
            insertedItems[0].DisplaySize = baseItem.DisplaySize;
            insertedItems[0].OSName = baseItem.OSName;
            insertedItems[0].OSVersion = baseItem.OSVersion;
            insertedItems[0].SIM = baseItem.SIM;
            insertedItems[0].SIMType = baseItem.SIMType;
            insertedItems[0].PhonebookCapacity = baseItem.PhonebookCapacity;
            insertedItems[0].Frequency = baseItem.Frequency;
            insertedItems[0].Weight = baseItem.Weight;
            insertedItems[0].RAM = baseItem.RAM;
            insertedItems[0].InternalStorage = baseItem.InternalStorage;
            insertedItems[0].ExternalStorage = baseItem.ExternalStorage;
            insertedItems[0].Wifi = baseItem.Wifi;
            insertedItems[0].GPRS = baseItem.GPRS;
            insertedItems[0].Browser = baseItem.Browser;
            insertedItems[0].EDGE = baseItem.EDGE;
            insertedItems[0].Supports3G = baseItem.Supports3G;
            insertedItems[0].Bluetooth = baseItem.Bluetooth;
            insertedItems[0].FlashAvailable = baseItem.FlashAvailable;
            insertedItems[0].Quert = baseItem.Quert;
            insertedItems[0].MusicPlayer = baseItem.MusicPlayer;
            insertedItems[0].TouchScreen = baseItem.TouchScreen;
            insertedItems[0].PrimaryCamera = baseItem.PrimaryCamera;
            insertedItems[0].SecondaryCamera = baseItem.SecondaryCamera;
            insertedItems[0].CameraSpecifications = baseItem.CameraSpecifications;
            insertedItems[0].AudioSpecifications = baseItem.AudioSpecifications;
            insertedItems[0].AudioFormat = baseItem.AudioFormat;
            insertedItems[0].VideoSpecifications = baseItem.VideoSpecifications;
            insertedItems[0].BatteryType = baseItem.BatteryType;
            insertedItems[0].TalkTime = baseItem.TalkTime;
            insertedItems[0].StandBy = baseItem.StandBy;
            insertedItems[0].SpecialFeature1 = baseItem.SpecialFeature1;
            insertedItems[0].SpecialFeature2 = baseItem.SpecialFeature2;
            insertedItems[0].SpecialFeature3 = baseItem.SpecialFeature3;
            insertedItems[0].Discount = baseItem.Discount;
            insertedItems[0].DiscountAvailable = baseItem.DiscountAvailable;
            insertedItems[0].OffersAvailable = baseItem.OffersAvailable;
            insertedItems[0].EMIAvailable = baseItem.EMIAvailable;
            insertedItems[0].ExchangeAvailable = baseItem.ExchangeAvailable;
            insertedItems[0].FinancingOptionsAvailable = baseItem.FinancingOptionsAvailable;
            insertedItems[0].Offer = baseItem.Offer;
            insertedItems[0].EMIMonths = baseItem.EMIMonths;
            insertedItems[0].ExchangeDetails = baseItem.ExchangeDetails;
            insertedItems[0].FinancerDetails = baseItem.FinancerDetails;
            insertedItems[0].VideoFormat = baseItem.VideoFormat;
            insertedItems[0].Verdict = baseItem.Verdict;
            insertedItems[0].Pros = baseItem.Pros;
            insertedItems[0].Cons = baseItem.Cons;
            insertedItems[0].IsHotItem = baseItem.IsHotItem;

            insertedItems[0].ItemBrand = baseItem.ItemBrand;
            insertedItems[0].ItemCateg = baseItem.ItemCateg;
            insertedItems[0].ItemModel = baseItem.ItemModel;
            insertedItems[0].LstComments = baseItem.LstComments;

            int i = 0;
            foreach (LotDBEntity.Models.BasicItem item in insertedItems)
            {
                if (i > 0)
                {
                    if (insertedItems[0].LstAccessories == null)
                        insertedItems[0].LstAccessories = new List<LotDBEntity.Models.BasicAccessories>();

                    if (item.LstAccessories.Count > 0)
                        insertedItems[0].LstAccessories.Add(item.LstAccessories[0]);
                    CartCnt++;
                }
                i++;
            }

            return insertedItems[0];
        }

        private static LotDBEntity.Models.BasicItem CopytoInsertedItem(LotDBEntity.Models.BasicItem baseItem, LotDBEntity.Models.BasicItem insertedItem)
        {
            if (insertedItem == null)
                insertedItem = new LotDBEntity.Models.BasicItem();

            insertedItem.SNo = baseItem.SNo;
            insertedItem.ItemName = baseItem.ItemName;
            insertedItem.ItemCode = baseItem.ItemCode;

            insertedItem.Length = baseItem.Length;
            insertedItem.Breadth = baseItem.Breadth;
            insertedItem.Height = baseItem.Height;

            insertedItem.ImageName = baseItem.ImageName;
            insertedItem.ImageLocation = baseItem.ImageLocation;
            insertedItem.DefaultPrice = baseItem.DefaultPrice;
            insertedItem.ItemColor = baseItem.ItemColor;
            insertedItem.Description = baseItem.Description;
            insertedItem.ProcessorType = baseItem.ProcessorType;
            insertedItem.ProcessorSpeed = baseItem.ProcessorSpeed;
            insertedItem.DisplayType = baseItem.DisplayType;
            insertedItem.DisplaySize = baseItem.DisplaySize;
            insertedItem.OSName = baseItem.OSName;
            insertedItem.OSVersion = baseItem.OSVersion;
            insertedItem.SIM = baseItem.SIM;
            insertedItem.SIMType = baseItem.SIMType;
            insertedItem.PhonebookCapacity = baseItem.PhonebookCapacity;
            insertedItem.Frequency = baseItem.Frequency;
            insertedItem.Weight = baseItem.Weight;
            insertedItem.RAM = baseItem.RAM;
            insertedItem.InternalStorage = baseItem.InternalStorage;
            insertedItem.ExternalStorage = baseItem.ExternalStorage;
            insertedItem.Wifi = baseItem.Wifi;
            insertedItem.GPRS = baseItem.GPRS;
            insertedItem.Browser = baseItem.Browser;
            insertedItem.EDGE = baseItem.EDGE;
            insertedItem.Supports3G = baseItem.Supports3G;
            insertedItem.Bluetooth = baseItem.Bluetooth;
            insertedItem.FlashAvailable = baseItem.FlashAvailable;
            insertedItem.Quert = baseItem.Quert;
            insertedItem.MusicPlayer = baseItem.MusicPlayer;
            insertedItem.TouchScreen = baseItem.TouchScreen;
            insertedItem.PrimaryCamera = baseItem.PrimaryCamera;
            insertedItem.SecondaryCamera = baseItem.SecondaryCamera;
            insertedItem.CameraSpecifications = baseItem.CameraSpecifications;
            insertedItem.AudioSpecifications = baseItem.AudioSpecifications;
            insertedItem.AudioFormat = baseItem.AudioFormat;
            insertedItem.VideoSpecifications = baseItem.VideoSpecifications;
            insertedItem.BatteryType = baseItem.BatteryType;
            insertedItem.TalkTime = baseItem.TalkTime;
            insertedItem.StandBy = baseItem.StandBy;
            insertedItem.SpecialFeature1 = baseItem.SpecialFeature1;
            insertedItem.SpecialFeature2 = baseItem.SpecialFeature2;
            insertedItem.SpecialFeature3 = baseItem.SpecialFeature3;
            insertedItem.Discount = baseItem.Discount;
            insertedItem.DiscountAvailable = baseItem.DiscountAvailable;
            insertedItem.OffersAvailable = baseItem.OffersAvailable;
            insertedItem.EMIAvailable = baseItem.EMIAvailable;
            insertedItem.ExchangeAvailable = baseItem.ExchangeAvailable;
            insertedItem.FinancingOptionsAvailable = baseItem.FinancingOptionsAvailable;
            insertedItem.Offer = baseItem.Offer;
            insertedItem.EMIMonths = baseItem.EMIMonths;
            insertedItem.ExchangeDetails = baseItem.ExchangeDetails;
            insertedItem.FinancerDetails = baseItem.FinancerDetails;
            insertedItem.VideoFormat = baseItem.VideoFormat;
            insertedItem.Verdict = baseItem.Verdict;
            insertedItem.Pros = baseItem.Pros;
            insertedItem.Cons = baseItem.Cons;
            insertedItem.IsHotItem = baseItem.IsHotItem;

            insertedItem.ItemBrand = baseItem.ItemBrand;
            insertedItem.ItemCateg = baseItem.ItemCateg;
            insertedItem.ItemModel = baseItem.ItemModel;
            insertedItem.LstComments = baseItem.LstComments;
            return insertedItem;
        }
    }
}
