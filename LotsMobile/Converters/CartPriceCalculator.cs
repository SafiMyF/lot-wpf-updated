﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class CartPriceCalculator:IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            decimal itemPrice = 0;
            string convType = System.Convert.ToString(parameter);
            if (convType != "T")
            {
                LotDBEntity.Models.BasicItem basicItem = value as LotDBEntity.Models.BasicItem;
                
                if (basicItem != null)
                {
                    if (basicItem.LstAccessories != null && basicItem.LstAccessories.Count > 0)
                        itemPrice = basicItem.LstAccessories.Sum(c => c.Price) + basicItem.DefaultPrice;
                    else
                        itemPrice = basicItem.DefaultPrice;
                }

                List<LotDBEntity.Models.BasicAccessories> basicAccessories = value as List<LotDBEntity.Models.BasicAccessories>;
                if (basicAccessories != null && basicAccessories.Count > 0)
                {
                    itemPrice = basicAccessories.Sum(c => c.Price);
                }
            }
            else
            {
                if (StoreCompare_Cart.lstCart.Count > 0)
                {
                    itemPrice = 0;
                    itemPrice = StoreCompare_Cart.lstCart.Sum(c => c.DefaultPrice);
                    foreach (LotDBEntity.Models.BasicItem item in StoreCompare_Cart.lstCart)
                    {
                        if (item.LstAccessories != null && item.LstAccessories.Count > 0)
                        {
                            itemPrice += item.LstAccessories.Sum(c => c.Price);
                        }
                    }
                }
            }
            return itemPrice;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
