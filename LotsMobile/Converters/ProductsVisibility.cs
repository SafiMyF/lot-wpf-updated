﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class ProductsVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string param=System.Convert.ToString(parameter);
            if (!string.IsNullOrEmpty(param))
            {
                if (param == "Product")
                {
                    List<LotDBEntity.Models.SimilarProducts> lstSimilarProducts = value as List<LotDBEntity.Models.SimilarProducts>;
                    if (lstSimilarProducts.Count > 0)
                        return Visibility.Visible;
                    else
                        return Visibility.Collapsed;
                }
                else 
                {
                    List<LotDBEntity.Models.BasicAccessories> lstAccessories = value as List<LotDBEntity.Models.BasicAccessories>;
                    if (lstAccessories.Count > 0)
                        return Visibility.Visible;
                    else
                        return Visibility.Collapsed;
                }
            }
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
