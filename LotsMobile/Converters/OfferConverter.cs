﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class OfferConverter:IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string value1 = System.Convert.ToString(values[0]);
            string value2 = System.Convert.ToString(values[1]);
            string value3 = System.Convert.ToString(values[2]);
            string value4 = System.Convert.ToString(values[3]);
            string value5 = System.Convert.ToString(values[4]);
            string value6 = System.Convert.ToString(values[5]);
            if ((!string.IsNullOrEmpty(value1)) || (!string.IsNullOrEmpty(value2)) || (!string.IsNullOrEmpty(value3)) || (!string.IsNullOrEmpty(value4)) || (!string.IsNullOrEmpty(value5)) || (!string.IsNullOrEmpty(value6)))
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
