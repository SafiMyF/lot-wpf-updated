﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace LotsMobile.Converters
{
    public class CartSelector : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string strImgLoc = value as string;
            string isAccessory = parameter as string;
            if (string.IsNullOrEmpty(isAccessory))
            {
                if (string.IsNullOrEmpty(strImgLoc))
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }
            else
            {
                if (!string.IsNullOrEmpty(strImgLoc))
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
