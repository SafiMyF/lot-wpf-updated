﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LotDBEntity;
using LotsMobile.Models;
using System.Windows.Media.Animation;
using System.Reflection;
using System.ComponentModel;
using System.Configuration;
using LotMobilesAutoSync.Models;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public LotDBEntity.DBFolder.LotDBContext DBContext;
        private string serverIP, portNum;
        //bool isScrollMoved = false;
        //bool isVideoPlayed = false;
        int PZindex;
        Configuration config;

        public MainWindow()
        {
            InitializeComponent();
            PZindex = 9999;
        }

        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnItem_TouchDown(object sender, TouchEventArgs e)
        {
            //if (!isScrollMoved)
            //{
            try
            {
                LogHelper.Logger.Info("Started When Button Item TouchDown In Main Window");
                parentPanel.Children.Clear();
                Listing lst = new Listing();
                isFromCart = false;
                parentPanel.Children.Add(lst);
                stkCartCompareBtnPanel.Visibility = Visibility.Visible;
                lst.myEventListingItem += lst_myEventListingItem;

                LogHelper.Logger.Info("Ended When Button Item TouchDown in MainWindow");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : btnItem_TouchDown: " + ex.Message, ex);
            }
                
            //}
            //isScrollMoved = false;
        }

        void lst_myEventListingItem(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    LotDBEntity.Models.BasicItem basicItem = btn.DataContext as LotDBEntity.Models.BasicItem;
                    ItemView iv = new ItemView();
                    if (!CheckChildren(basicItem))
                    {
                        //isFromCart = false;
                        LogHelper.Logger.Info("Started Checking Children is Available for ContentPanel");
                        iv.myEventCartCntUpdate -= iv_myEventCartCntUpdate;
                        iv.myEventCartCntUpdate += iv_myEventCartCntUpdate;
                        iv.MyEventCmpreCntUpdate += iv_MyEventCmpreCntUpdate;
                        iv.myEventCartRefresh += iv_myEventCartRefresh;
                        iv.refreshIsfromCartVal += iv_refreshIsfromCartVal;
                        iv.IsManipulationEnabled = true;
                        iv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        iv.BasicItem = basicItem;
                        iv.launchSimilarProduct += lst_myEventListingItem;
                        iv.launchAccessory += iv_launchAccessory;
                        iv.closeBtnClicked += iv_closeBtnClicked;
                        iv.isFromCart = isFromCart;
                        parentPanel.Children.Add(iv);
                        iv.BringIntoView();

                        Panel.SetZIndex(iv, PZindex);
                        LogHelper.Logger.Info("Ended Checking Children is Available for ContentPanel");
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.Logger.ErrorException("Exception: MainWindow : lst_myEventListingItem: " + ex.Message, ex);
            }
        }

        void iv_refreshIsfromCartVal(object sender, EventArgs e)
        {
            isFromCart = true;
        }
        void iv_MyEventCmpreCntUpdate(object sender, EventArgs e)
        {
            lblCompareCount.Content = StoreCompare_Cart.CmpreCnt;
        }
        void iv_myEventCartCntUpdate(object sender, EventArgs e)
        {
            int count = StoreCompare_Cart.lstCart.Count(c => c.ItemName != null);
            StoreCompare_Cart.lstCart.ToList().ForEach(c=>
                {
                    if (c.LstAccessories != null)
                        count += c.LstAccessories.Count;
                });
            lblCartCount.Content = count;
        }

        private bool CheckChildren(LotDBEntity.Models.BasicItem basicItem, bool isFromCart = false)
        {
            foreach (Control ctrl in parentPanel.Children)
            {
                if (ctrl is ItemView)
                {
                    ItemView iv = ctrl as ItemView;
                    LotDBEntity.Models.BasicItem sample = iv.DataContext as LotDBEntity.Models.BasicItem;

                    if (basicItem.SNo == sample.SNo)
                    {
                        iv.Visibility = Visibility.Visible;
                        iv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        Panel.SetZIndex(iv, PZindex++);
                        iv.BringIntoView();
                        return true;
                    }
                }
            }
            return false;
        }
        void iv_IvComment(object sender, EventArgs e)
        {
            //UserComment userComment = new UserComment();
            //userComment.basicItem = (LotDBEntity.Models.BasicItem)sender;
            //userComment.ShowDialog();

            //UserCommentPanel.DataContext = (LotDBEntity.Models.BasicItem)sender;
            //UserCommentPanel.Visibility = Visibility.Visible;
        }

        void iv_launchAccessory(object sender, EventArgs e)
        {
            try
            {
                List<object> lstObj = sender as List<object>;
                long sno = -1;
                long.TryParse(Convert.ToString(lstObj[1]), out sno);
                bool isFromCart = false;
                bool.TryParse(Convert.ToString(lstObj[2]), out isFromCart);
                LotDBEntity.Models.BasicAccessories basicAccessory = lstObj[0] as LotDBEntity.Models.BasicAccessories;
                LogHelper.Logger.Info("Started Checking Children of Basic Accessory");
                if (!CheckAccessoryChildren(basicAccessory))
                {
                    AccessoriesView Aiv = new AccessoriesView();
                    Aiv.isFromCart = isFromCart;
                    Aiv.IsManipulationEnabled = true;
                    Aiv.myEventCartCntUpdate += Aiv_myEventCartCntUpdate;
                    Aiv.myEventCartRefresh += Aiv_myEventCartRefresh;
                    Aiv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                    Aiv.basicAccessory = basicAccessory;
                    Aiv.ItemSNo = sno;
                    Aiv.AccessoryCloseBtnClicked += Aiv_AccessoryCloseBtnClicked;

                    parentPanel.Children.Add(Aiv);
                    Aiv.BringIntoView();
                    Panel.SetZIndex(Aiv, PZindex);
                    Aiv.AccessoryComment += Aiv_AccessoryComment;
                    LogHelper.Logger.Info("Ended Checking Children of Basic Accessory");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : iv_launchAccessory: " + ex.Message, ex);
            }
            

        }
        void Aiv_myEventCartRefresh(object sender, EventArgs e)
        {
            if (isFromCart)
                ReInitilizeCart();
        }
        void Aiv_myEventCartCntUpdate(object sender, EventArgs e)
        {
            lblCartCount.Content = StoreCompare_Cart.CartCnt;
        }


        private bool CheckAccessoryChildren(LotDBEntity.Models.BasicAccessories basicAccessory)
        {
            foreach (Control ctrl in parentPanel.Children)
            {
                if (ctrl is AccessoriesView)
                {
                    AccessoriesView iv = ctrl as AccessoriesView;
                    LotDBEntity.Models.BasicAccessories lstSample = iv.DataContext as LotDBEntity.Models.BasicAccessories;

                    if (basicAccessory.SNo == lstSample.SNo)
                    {
                        iv.Visibility = Visibility.Visible;
                        iv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        iv.BringIntoView();
                        Panel.SetZIndex(iv, PZindex++);
                        return true;
                    }
                }
            }
            return false;
        }

        void Aiv_AccessoryComment(object sender, EventArgs e)
        {
            UserCommentPanel.Visibility = Visibility.Visible;
        }

        void Aiv_AccessoryCloseBtnClicked(object sender, EventArgs e)
        {
            AccessoriesView iView = (AccessoriesView)sender;
            parentPanel.Children.Remove(iView);
        }

        void iv_closeBtnClicked(object sender, EventArgs e)
        {
            ItemView iView = (ItemView)sender;
            parentPanel.Children.Remove(iView);
        }

        private void btnHome_TouchDown_1(object sender, TouchEventArgs e)
        {
            //Apps.lotContext = new LotDBEntity.DBFolder.LotDBContext(Utilities.connectionString);
            //isVideoPlayed = false;
            PZindex = 9999;
            HomePanel.Visibility = Visibility.Visible;
            btnAdminSetting.Visibility = Visibility.Visible;
            Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
            HomeSB.Seek(TimeSpan.Zero);

            parentPanel.Children.Clear();
            btnOfferOfTheDay.Visibility = Visibility.Collapsed;
            btnShopMore.Visibility = Visibility.Collapsed;
            btnFindAccessories.Visibility = Visibility.Collapsed;
            stkCartCompareBtnPanel.Visibility = Visibility.Hidden;
            StoreCompare_Cart.CartCnt = 0;
            lblCartCount.Content = StoreCompare_Cart.CartCnt.ToString();
            StoreCompare_Cart.lstCart.Clear();
            StoreCompare_Cart.CmpreCnt = 0;
            lblCompareCount.Content = StoreCompare_Cart.CmpreCnt.ToString();
            StoreCompare_Cart.lstCompare.Clear();
        }

        InactivityTimer _inactivityTimer;
        LotDBEntity.Models.BasicItem screenSaverItem = null;
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            long selectedId, totSeconds;
            Int64.TryParse(Convert.ToString(config.AppSettings.Settings["ItemID"].Value), out selectedId);
            Int64.TryParse(Convert.ToString(config.AppSettings.Settings["ScreenSaverTimeSpan"].Value), out totSeconds);
            serverIP = Convert.ToString(config.AppSettings.Settings["AutoSyncIP"].Value);
            portNum = Convert.ToString(config.AppSettings.Settings["AutoSyncPort"].Value);
            Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
            HomeSB.Begin();

            this.ManipulationStarting -= Window_Loaded_ManipulationStarting;
            this.ManipulationDelta -= Window_Loaded_ManipulationDelta;
            this.ManipulationStarting += Window_Loaded_ManipulationStarting;
            this.ManipulationDelta += Window_Loaded_ManipulationDelta;
            //this.Closing += MainWindow_Closing;
            
            if (totSeconds > 10)
            {
                _inactivityTimer = new InactivityTimer(TimeSpan.FromSeconds(totSeconds));
                _inactivityTimer.Inactivity += _inactivityTimer_Inactivity;
                
                screenSaverItem = Apps.lotContext.BasicItems.Where(c => c.SNo == selectedId).FirstOrDefault();
            }
        }

        void _inactivityTimer_Inactivity(object sender, EventArgs e)
        {
            ScreenView screenView = new ScreenView();
            if (screenSaverItem != null)
            {
                screenView.BasicItem = screenSaverItem;
                screenView.ShowDialog();
            }
        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (Utilities.launchAppImmediately)
                System.Diagnostics.Process.Start(@"C:\Main Lot\LotsMobile.exe");
        }

        ManipulationModes currentMode = ManipulationModes.All;
        private void Window_Loaded_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }

        private void Window_Loaded_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            MatrixTransform xform = element.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;
            ManipulationDelta delta = args.DeltaManipulation;
            Point center = args.ManipulationOrigin;
            matrix.Translate(-center.X, -center.Y);
            matrix.Scale(delta.Scale.X, delta.Scale.Y);
            matrix.Translate(center.X, center.Y);
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            xform.Matrix = matrix;
            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        private void btnCompare_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Started Button Compare Touchdown in MainWindow");
                Compare cmp = new Compare();
                cmp.myEventCompareCartUpdate += cmp_myEventCompareCartUpdate;
                cmp.myEventCompareDeleteUpdate += cmp_myEventCompareDeleteUpdate;
                parentPanel.Children.Clear();
                parentPanel.Children.Add(cmp);
                btnShopMore.Visibility = Visibility.Visible;
                cmp.myEventCmpPhotos += cmp_myEventCmpPhotos;
                cmp.myEventCmpVideo += cmp_myEventCmpVideo;
                LogHelper.Logger.Info("Ended Button Compare Touchdown in MainWindow");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : btnCompare_TouchDown_1: " + ex.Message, ex);
            }
            
        }

        void cmp_myEventCmpVideo(object sender, EventArgs e)
        {
            CompareVideo cmpVdeo = new CompareVideo();
            List<LotDBEntity.Models.BasicItem> lstbasicItems = sender as List<LotDBEntity.Models.BasicItem>;
            if (lstbasicItems != null && lstbasicItems.Count > 0)
            {
                cmpVdeo.txtProductName1.Text = lstbasicItems[0].ItemName;
                cmpVdeo.txtProductName2.Text = lstbasicItems[1].ItemName;
                if (lstbasicItems[0].LstImgsAndVideos.ToList().Exists(c => c.IsVideo == true))
                {
                    cmpVdeo.Video1.Source = new Uri(lstbasicItems[0].LstImgsAndVideos.Where(c => c.IsVideo == true).FirstOrDefault().Location);
                }
                else
                {
                    //Check First Item Video. if null then display NoVideo.png and hide play-pause-stop button panel
                    cmpVdeo.Video1.Source = null;
                    cmpVdeo.vdoFirstMob.Visibility = Visibility.Visible;
                    cmpVdeo.stkBtnPanel1.Visibility = Visibility.Collapsed;
                }

                if (lstbasicItems[1].LstImgsAndVideos.ToList().Exists(c => c.IsVideo == true))
                {
                    cmpVdeo.Video2.Source = new Uri(lstbasicItems[1].LstImgsAndVideos.Where(c => c.IsVideo == true).FirstOrDefault().Location);
                }
                else
                {
                    //Check Second Item Video. if null then display NoVideo.png and hide play-pause-stop button panel
                    cmpVdeo.Video2.Source = null;
                    cmpVdeo.vdoSecondMob.Visibility = Visibility.Visible;
                    cmpVdeo.stkBtnPanel2.Visibility = Visibility.Collapsed;
                }
            }


            parentPanel.Children.Add(cmpVdeo);
            cmpVdeo.BringIntoView();
            Panel.SetZIndex(cmpVdeo, 99999);
            cmpVdeo.myEventCloseVideo += cmpVdeo_myEventCloseVideo;
        }

        void cmpVdeo_myEventCloseVideo(object sender, EventArgs e)
        {
            CompareVideo cmpVdeo = (CompareVideo)sender;
            parentPanel.Children.Remove(cmpVdeo); 
        }
        
        void cmp_myEventCmpPhotos(object sender, EventArgs e)
        {
            ComparePV cmpPV = new ComparePV();
            List<LotDBEntity.Models.BasicItem> lstbasicItems = sender as List<LotDBEntity.Models.BasicItem>;
            if (lstbasicItems != null && lstbasicItems.Count > 0)
            {
                cmpPV.txtProductName1.Text = lstbasicItems[0].ItemName.ToString();
                cmpPV.txtProductName2.Text = lstbasicItems[1].ItemName.ToString();
                cmpPV.ListingImageFirstContext.ItemsSource = lstbasicItems[0].LstImgsAndVideos.Where(c => c.IsVideo == false).ToList();
                cmpPV.ListingImageSecondContext.ItemsSource = lstbasicItems[1].LstImgsAndVideos.Where(c => c.IsVideo == false).ToList();
                //Check First Item Image is null. if null display noImages.png
                if (lstbasicItems[0].LstImgsAndVideos.Where(c => c.IsVideo == false).ToList().Count == 0)
                    cmpPV.imgFirstMob.Visibility = Visibility.Visible;
                else
                    cmpPV.imgFirstMob.Visibility = Visibility.Collapsed;

                //Check Second Item Image is null. if null display noImages.png
                if (lstbasicItems[1].LstImgsAndVideos.Where(c => c.IsVideo == false).ToList().Count == 0)
                    cmpPV.imgSecondMob.Visibility = Visibility.Visible;
                else
                    cmpPV.imgSecondMob.Visibility = Visibility.Collapsed;

            }
            parentPanel.Children.Add(cmpPV);
            cmpPV.BringIntoView();
            Panel.SetZIndex(cmpPV, 99999);
            cmpPV.myEventClosePV += cmpPV_myEventClosePV;
        }

        void cmpPV_myEventClosePV(object sender, EventArgs e)
        {
            ComparePV cmpPV = (ComparePV)sender;
            parentPanel.Children.Remove(cmpPV);
        }

        void cmp_myEventCompareDeleteUpdate(object sender, EventArgs e)
        {
            lblCompareCount.Content = StoreCompare_Cart.CmpreCnt;
        }

        void cmp_myEventCompareCartUpdate(object sender, EventArgs e)
        {
            LotDBEntity.Models.BasicItem grdCompareItem = sender as LotDBEntity.Models.BasicItem;
            StoreCompare_Cart.AddCart<LotDBEntity.Models.BasicItem>(grdCompareItem, grdCompareItem.SNo);
            lblCartCount.Content = StoreCompare_Cart.CartCnt;
            lblCompareCount.Content = StoreCompare_Cart.CmpreCnt;
        }
        private void btnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
            ReInitilizeCart();
        }

        private void ReInitilizeCart()
        {
            double verticalOffset = 0;
            foreach (var item in parentPanel.Children)
            {
                if (item is Cart)
                {
                    Cart crtItem = item as Cart;
                    verticalOffset = crtItem.ScrollViewer.VerticalOffset;
                }
            }

            Cart crt = new Cart();
            isFromCart = true;
            crt.verticalOffset = verticalOffset;
            parentPanel.Children.Clear();
            parentPanel.Children.Add(crt);
            btnShopMore.Visibility = Visibility.Visible;
            btnFindAccessories.Visibility = Visibility.Visible;

            crt.myEventCartCntUpdate += iv_myEventCartCntUpdate;
            crt.MyEventCartItemSelect += crt_MyEventCartItemSelect;
            crt.myEventCartRefresh += crt_myEventCartRefresh;
        }

        void crt_myEventCartRefresh(object sender, EventArgs e)
        {
            ReInitilizeCart();
        }

        bool isFromCart = false;
        void crt_MyEventCartItemSelect(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    LogHelper.Logger.Info("Started Checking Children is Available in CartPage Or Not");
                    LotDBEntity.Models.BasicItem basicItem = btn.DataContext as LotDBEntity.Models.BasicItem;
                    LotDBEntity.Models.BasicItem basicFetchedItem = Apps.lotContext.BasicItems.Where(c => c.SNo == basicItem.SNo).FirstOrDefault();
                    LotDBEntity.Models.SimilarProducts Similar = btn.DataContext as LotDBEntity.Models.SimilarProducts;

                    if (!CheckChildren(basicItem, true))
                    {
                        isFromCart = true;
                        ItemView iv = new ItemView();

                        iv.myEventCartCntUpdate -= iv_myEventCartCntUpdate;

                        iv.myEventCartCntUpdate += iv_myEventCartCntUpdate;
                        iv.MyEventCmpreCntUpdate += iv_MyEventCmpreCntUpdate;
                        iv.myEventCartRefresh += iv_myEventCartRefresh;
                        iv.refreshIsfromCartVal += iv_refreshIsfromCartVal;
                        iv.IsManipulationEnabled = true;
                        iv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        iv.BasicItem = basicFetchedItem;
                        iv.isFromCart = true;

                        iv.launchSimilarProduct += lst_myEventListingItem;
                        iv.launchAccessory += iv_launchAccessory;
                        iv.closeBtnClicked += iv_closeBtnClicked;

                        parentPanel.Children.Add(iv);
                        iv.BringIntoView();
                        Panel.SetZIndex(iv, PZindex);
                        LogHelper.Logger.Info("Ended Checking Children is Available in CartPage Or Not");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : crt_MyEventCartItemSelect: " + ex.Message, ex);
            }
           
        }

        void iv_myEventCartRefresh(object sender, EventArgs e)
        {
            if (isFromCart)
                ReInitilizeCart();
        }

        private void scrollViewer_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            //isScrollMoved = true;
        }

        private void btnCloseComment_TouchDown_1(object sender, TouchEventArgs e)
        {
            UserCommentPanel.Visibility = Visibility.Collapsed;
            txtUserName.Text = "";
            txtUserContactNo.Text = "";
            txtUserComment.Text = "";
        }

        UserOptions Uopts;
        private void grdFindMatch_TouchDown_1(object sender, TouchEventArgs e)
        {
            btnAdminSetting.Visibility = Visibility.Hidden;
            LoadUserOptions();
        }

        void Uoffs_myEventFindUrMatch(object sender, EventArgs e)
        {
            LoadUserOptions();
        }

        private void LoadUserOptions()
        {
            try
            {
                LogHelper.Logger.Info("Starts LoadUserOptions() in MainWindowPage");
                Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
                HomeSB.Stop();

                HomePanel.Visibility = Visibility.Hidden;
                stkCartCompareBtnPanel.Visibility = Visibility.Collapsed;
                btnFindAccessories.Visibility = Visibility.Collapsed;
                Uopts = new UserOptions();
                parentPanel.Children.Clear();
                parentPanel.Children.Add(Uopts);
                Uopts.myEventUserOption += Uopts_myEventUserOption;
                Uopts.myEventMoreOffer += Uopts_myEventMoreOffer;
                Uopts.myEventAccListing += Uopts_myEventAccListing;
                LogHelper.Logger.Info("Ends LoadUserOptions() in MainWindowPage");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : LoadUserOptions(): " + ex.Message, ex);
            }
            
        }

        void Uopts_myEventAccListing(object sender, EventArgs e)
        {
            LoadAccessoryListing();      
        }

        void accList_myEventAccListingItem(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    LotDBEntity.Models.BasicAccessories basicAcc = btn.DataContext as LotDBEntity.Models.BasicAccessories;
                    AccessoriesView aiv = new AccessoriesView();
                    if (!CheckChildren(basicAcc))
                    {
                        isFromCart = false;
                        LogHelper.Logger.Info("Started Checking Children is Available for ContentPanel");
                        aiv.myEventCartCntUpdate -= iv_myEventCartCntUpdate;
                        aiv.myEventCartCntUpdate += iv_myEventCartCntUpdate;
                        aiv.myEventCartRefresh += iv_myEventCartRefresh;
                        aiv.IsManipulationEnabled = true;
                        aiv.basicAccessory = basicAcc;
                        aiv.ItemSNo = new Random().Next(10000, 65000);
                        aiv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        aiv.AccessoryCloseBtnClicked += aiv_AccessoryCloseBtnClicked;

                        parentPanel.Children.Add(aiv);
                        aiv.BringIntoView();

                        Panel.SetZIndex(aiv, PZindex);
                        LogHelper.Logger.Info("Ended Checking Children is Available for ContentPanel");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : lst_myEventListingItem: " + ex.Message, ex);
            }
        }

        void aiv_AccessoryCloseBtnClicked(object sender, EventArgs e)
        {
            AccessoriesView iView = (AccessoriesView)sender;
            parentPanel.Children.Remove(iView);
        }

        private bool CheckChildren(LotDBEntity.Models.BasicAccessories basicAcc)
        {

            foreach (Control ctrl in parentPanel.Children)
            {
                if (ctrl is AccessoriesView)
                {
                    AccessoriesView iv = ctrl as AccessoriesView;
                    LotDBEntity.Models.BasicAccessories sample = iv.DataContext as LotDBEntity.Models.BasicAccessories;

                    if (basicAcc.SNo == sample.SNo)
                    {
                        iv.Visibility = Visibility.Visible;
                        iv.RenderTransform = new MatrixTransform(0.9, 0, 0, 0.9, 10, 10);
                        Panel.SetZIndex(iv, PZindex++);
                        iv.BringIntoView();
                        return true;
                    }
                }
            }
            return false;
        }

        void Uopts_myEventMoreOffer(object sender, EventArgs e)
        {
            LoadUserOffers();
        }

        UserOffers Uoffs;
        private void grdOffers_TouchDown_1(object sender, TouchEventArgs e)
        {
            btnAdminSetting.Visibility = Visibility.Hidden;
            LoadUserOffers();
        }

        private void btnOfferOfTheDay_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadUserOffers();
        }
        
        private void LoadUserOffers()
        {
            try
            {
                LogHelper.Logger.Info("Starts LoadUSerOffers Function in MainWindow Page");
                Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
                HomeSB.Stop();

                HomePanel.Visibility = Visibility.Collapsed;
                btnOfferOfTheDay.Visibility = Visibility.Collapsed;
                btnShopMore.Visibility = Visibility.Collapsed;
                btnFindAccessories.Visibility = Visibility.Visible;
                stkCartCompareBtnPanel.Visibility = Visibility.Collapsed;
                Uoffs = new UserOffers();
                Uoffs.myEventSelectedMatch += Uoffs_myEventSelectedMatch;
                parentPanel.Children.Clear();
                parentPanel.Children.Add(Uoffs);
                Uoffs.myEventFindUrMatch += Uoffs_myEventFindUrMatch;
                LogHelper.Logger.Info("Ends LoadUSerOffers Function in MainWindow Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : LoadUserOffers(): " + ex.Message, ex);
            }
            
        }

        void Uoffs_myEventSelectedMatch(object sender, EventArgs e)
        {
            List<LotDBEntity.Models.BasicItem> lstBasicItems = sender as List<LotDBEntity.Models.BasicItem>;
            LoadItemListingPage(lstBasicItems);
        }

        UserOptionModels useroptionModel;
        void Uopts_myEventUserOption(object sender, EventArgs e)
        {
            useroptionModel = sender as UserOptionModels;

            if (useroptionModel != null)
            {
                LoadItemListingPage(useroptionModel.lstChoosedItems, true);
            }
            else
            {
                LoadItemListingPage();
            }

        }

        private void btnShopMore_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadItemListingPage();
        }

        private void LoadItemListingPage(List<LotDBEntity.Models.BasicItem> lstBasicItems = null, bool isRefresh = false)
        {
            btnOfferOfTheDay.Visibility = Visibility.Visible;
            btnShopMore.Visibility = Visibility.Collapsed;
            stkCartCompareBtnPanel.Visibility = Visibility.Visible;
            btnFindAccessories.Visibility = Visibility.Visible;
            parentPanel.Children.Clear();
            Listing lst = new Listing();
            lst.isRefresh = isRefresh;
            isFromCart = false;

            if (lstBasicItems != null)
                lst.lstChoosedItems = lstBasicItems;

            if (useroptionModel != null && isRefresh)
            {
                lst.selectedPriceIndex = useroptionModel.selectedPriceIndex;
                lst.selectedItemBrand = useroptionModel.SelectedItemBrand;
            }
            else
            {
                
            }

            parentPanel.Children.Add(lst);
            lst.myEventListingItem += lst_myEventListingItem;
        }

        private void btnCommentSave_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("When Button Save Comments Touches Started");
                LotDBEntity.Models.BasicItem basicItem = UserCommentPanel.DataContext as LotDBEntity.Models.BasicItem;
                if (basicItem.LstComments == null)
                {
                    LotDBEntity.Models.Comments comment = new LotDBEntity.Models.Comments();
                    comment.CustomerName = txtUserName.Text;
                    comment.CustomerMobile = txtUserContactNo.Text;
                    comment.Comment = txtUserComment.Text;

                    List<LotDBEntity.Models.Comments> lstComments = new List<LotDBEntity.Models.Comments>();
                    lstComments.Add(comment);
                    basicItem.LstComments = lstComments;
                    Apps.lotContext.SaveChanges();
                }
                else
                {
                    LotDBEntity.Models.Comments comment = new LotDBEntity.Models.Comments();
                    comment.CustomerName = txtUserName.Text;
                    comment.CustomerMobile = txtUserContactNo.Text;
                    comment.Comment = txtUserComment.Text;

                    basicItem.LstComments.Add(comment);
                    Apps.lotContext.SaveChanges();
                }
                LogHelper.Logger.Info("When Button Save Comments Touches Ended");
                UserCommentPanel.Visibility = Visibility.Collapsed;
                txtUserName.Text = "";
                txtUserContactNo.Text = "";
                txtUserComment.Text = "";
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: MainWindow : btnCommentSave_TouchDown_1: " + ex.Message, ex);
            }
            
        }
        private void brdVideoSkip_TouchDown_1(object sender, TouchEventArgs e)
        {
            HomePanelVideo.Visibility = Visibility.Hidden;
            HomeVideo.Stop();
        }

        private void imgBg_TouchDown_1(object sender, TouchEventArgs e)
        {
            //if (isVideoPlayed == false)
            //{
            //    HomePanelVideo.Visibility = Visibility.Visible;
            //    HomeVideo.Play();
            //    isVideoPlayed = true;
            //}

            HomePanelVideo.Visibility = Visibility.Visible;
            HomeVideo.Play();
        }

        private void HomeVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            HomePanelVideo.Visibility = Visibility.Hidden;
            HomeVideo.Stop();
        }


        AdminLogin AdmLogin;
        private void btnAdminSetting_TouchDown_1(object sender, TouchEventArgs e)
        {
            LoadAdminLogin();
        }

        private void LoadAdminLogin()
        {
            btnAdminSetting.Visibility = Visibility.Hidden;
            AdmLogin = new AdminLogin();
            Settings.Children.Clear();
            Settings.Children.Add(AdmLogin);
            AdmLogin.myEventCloseAdminSetting += AdmLogin_myEventCloseAdminSetting;
            AdmLogin.myEventLoginAdmin += AdmLogin_myEventLoginAdmin;
        }


        private void LoadAdminSettings()
        {
            AdmSetting = new AdminSettings();
            Settings.Children.Clear();
            Settings.Children.Add(AdmSetting);
            AdmSetting.myEventAppDetails += AdmSetting_myEventAppDetails;
            AdmSetting.myEventAppSync += AdmSetting_myEventAppSync;
            AdmSetting.myEventLogoutSettings += AdmSetting_myEventLogoutSettings;
            AdmSetting.myEventSyncParameters += AdmSetting_myEventSyncParameters;
        }

        void AdmSetting_myEventSyncParameters(object sender, EventArgs e)
        {
            SyncParameters syncPara = new SyncParameters();
            Settings.Children.Clear();
            Settings.Children.Add(syncPara);
            syncPara.myEventBacktoAdminSettings += syncPara_myEventBacktoAdminSettings;
        }

        void syncPara_myEventBacktoAdminSettings(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        void AdmSetting_myEventLogoutSettings(object sender, EventArgs e)
        {
            Settings.Children.Clear();
            btnAdminSetting.Visibility = Visibility.Visible;
        }

        void AdmSetting_myEventAppSync(object sender, EventArgs e)
        {
            LoadSync();
        }

        private void LoadSync()
        {
            try
            {
                LogHelper.Logger.Info("Starts LoadSync()");
                SetAutoSync setsync = new SetAutoSync();
                Settings.Children.Clear();
                Settings.Children.Add(setsync);
                setsync.myEventBacktoAdminSettings += setsync_myEventBacktoAdminSettings;
                setsync.autoSyncRefresh += setsync_autoSyncRefresh;
                LogHelper.Logger.Info("Ends LoadSync()");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :LoadSync(): " + ex.Message, ex);
            }

        }

        void setsync_autoSyncRefresh(object sender, EventArgs e)
        {
            InitiateAutoSync();
        }

        void setsync_myEventBacktoAdminSettings(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        void AdmLogin_myEventLoginAdmin(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        AdminSettings AdmSetting;
        void AdmLogin_myEventCloseAdminSetting(object sender, EventArgs e)
        {
            btnAdminSetting.Visibility = Visibility.Visible;
            Settings.Children.Clear();
        }

        //AppDetails AppDets;
        void AdmSetting_myEventAppDetails(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts AdmSetting_myEventAppDetails");
                PickScreenPage ps = new PickScreenPage();
                Settings.Children.Clear();
                Settings.Children.Add(ps);
                ps.myEventBacktoAdminSetting += ps_myEventBacktoAdminSetting;
                //AppDets.startManualSync += AppDets_startManualSync;
                //AppDets.myEventBacktoAdminSetting += AppDets_myEventBacktoAdminSetting;
                LogHelper.Logger.Info("Ends AdmSetting_myEventAppDetails");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :AdmSetting_myEventAppDetails: " + ex.Message, ex);
            }

        }

        void ps_myEventBacktoAdminSetting(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        void AppDets_startManualSync(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts AppDets_startManualSync");
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                syncProcessPanel.Visibility = Visibility.Visible;
                if (bgWorker == null)
                {
                    syncProcessPanel.Visibility = Visibility.Visible;
                    bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += bgWorker_DoWork;
                    bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                    bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
                    bgWorker.WorkerReportsProgress = true;
                    bgWorker.WorkerSupportsCancellation = true;
                    bgWorker.RunWorkerAsync();
                }
                LogHelper.Logger.Info("Ends AppDets_startManualSync");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage :AppDets_startManualSync: " + ex.Message, ex);
            }

        }

        void AppDets_myEventBacktoAdminSetting(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Close();
        }
        DispatcherTimer timer = null;
        private void InitiateAutoSync()
        {
            try
            {
               
                    LogHelper.Logger.Info("Started InitiateAutoSync Function");
                    config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                    if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                    {
                        if (timer != null)
                        {
                            timer.Stop();
                            timer = null;
                        }
                        DateTime fromDate = DateTime.Now;
                        DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);

                        TimeSpan tspan = fetchSyncDate - fromDate;

                        if (tspan.Seconds < 0)
                        {
                            fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);
                            tspan = fetchSyncDate - fromDate;
                        }

                        if (timer == null)
                        {
                            timer = new DispatcherTimer();
                            timer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                            timer.Tick -= timer_Tick;
                            timer.Tick += timer_Tick;
                            timer.Start();
                        }
                    }
                LogHelper.Logger.Info("Ended InitiateAutoSync Function");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : InitiateAutoSync: " + ex.Message, ex);
            }

        }

        BackgroundWorker bgWorker;
        string exceptionMsg = string.Empty;
        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts TimerTick");
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                syncProcessPanel.Visibility = Visibility.Visible;
                if (bgWorker == null)
                {
                    bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += bgWorker_DoWork;
                    bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                    bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
                    bgWorker.WorkerReportsProgress = true;
                    bgWorker.WorkerSupportsCancellation = true;
                    bgWorker.RunWorkerAsync();
                }
                timer.Stop();
                LogHelper.Logger.Info("Ends TimerTick");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : timer_Tick: " + ex.Message, ex);

            }
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts bgWorker ProgressChanged");
                ProgressNotification progressNotify = e.UserState as ProgressNotification;
                PrgLoadingBar.Minimum = 0;
                PrgLoadingBar.Maximum = progressNotify.ProductsCount;
                PrgLoadingBar.Value = progressNotify.CurrentProduct;
                lblLoadingText.Text = progressNotify.UpdatedMessage;
                LogHelper.Logger.Info("Ends bgWorker ProgressChanged");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: HomePage : bgWorker_ProgressChanged: " + ex.Message, ex);
            }

        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(exceptionMsg))
                LogHelper.Logger.Info("Auto Sync Exception:" + exceptionMsg);
            else
                Apps.lotContext = new LotDBEntity.DBFolder.LotDBContext(Utilities.connectionString);
            syncProcessPanel.Visibility = Visibility.Hidden;
            bgWorker = null;
            timer.Stop();
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts bgWorker_DoWork");
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                serverIP = Convert.ToString(config.AppSettings.Settings["AutoSyncIP"].Value);
                portNum = Convert.ToString(config.AppSettings.Settings["AutoSyncPort"].Value);

                LotMobilesAutoSync.AutoSync autoSync = new LotMobilesAutoSync.AutoSync(serverIP, portNum, Utilities.storingPath);
                autoSync.syncProgressEvent += autosync_progressNotification;
                autoSync.ftpProgressEvent += autoSync_ftpProgressEvent;
                
                autoSync.StartDBSync();
                autoSync.StartFTPSync();
                LogHelper.Logger.Info("Ended bgWorker_DoWork");
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
                LogHelper.Logger.ErrorException("Exception: HomePage : bgWorker_DoWork: " + ex.Message, ex);
            }
        }

        void autoSync_ftpProgressEvent(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(1, sender);
        }

        void autosync_progressNotification(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(1, sender);
        }

        private void grdServices_TouchDown_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts ValueAddedServices in MainWindowPage");
            Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
            HomeSB.Stop();
            btnAdminSetting.Visibility = Visibility.Collapsed;
            HomePanel.Visibility = Visibility.Hidden;
            stkCartCompareBtnPanel.Visibility = Visibility.Collapsed;
            ValueAddedServises VAS = new ValueAddedServises();
            parentPanel.Children.Clear();
            parentPanel.Children.Add(VAS);
            VAS.myEventVAStoHome += VAS_myEventVAStoHome;
            LogHelper.Logger.Info("Ends ValueAddedServices in MainWindowPage");
        }

        void VAS_myEventVAStoHome(object sender, EventArgs e)
        {
            PZindex = 9999;
            HomePanel.Visibility = Visibility.Visible;
            btnAdminSetting.Visibility = Visibility.Visible;
            Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
            HomeSB.Seek(TimeSpan.Zero);
            parentPanel.Children.Clear();
        }
        
        private void btnFindAccessories_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            LoadAccessoryListing();
        }

        private void grdAccessories_TouchDown_1(object sender, TouchEventArgs e)
        {
            LogHelper.Logger.Info("Starts FindAccessories in MainWindowPage");
            Storyboard HomeSB = this.TryFindResource("LotHomeSlider_SB") as Storyboard;
            HomeSB.Stop();
            btnAdminSetting.Visibility = Visibility.Collapsed;
            HomePanel.Visibility = Visibility.Hidden;
            LoadAccessoryListing();
            LogHelper.Logger.Info("Ends FindAccessories in MainWindowPage");
        }

        private void LoadAccessoryListing()
        {
            AccessoryListing accList = new AccessoryListing();
            parentPanel.Children.Clear();
            parentPanel.Children.Add(accList);
            stkCartCompareBtnPanel.Visibility = Visibility.Visible;
            btnOfferOfTheDay.Visibility = Visibility.Visible;
            btnShopMore.Visibility = Visibility.Visible;
            btnFindAccessories.Visibility = Visibility.Collapsed;
            accList.myEventAccListingItem += accList_myEventAccListingItem;
        }
    }
}
