﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LotsMobile
{
    /// <summary>
    /// Interaction logic for ComparePV.xaml
    /// </summary>
    public partial class ComparePV : UserControl
    {
        public ComparePV()
        {
            InitializeComponent();
        }

        public event EventHandler myEventClosePV;
        private void btnClosePV_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventClosePV(this, null);
        }
        
        private void BtnItem_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts BtnItem_TouchDown_1 in ComparePV Page");
                Button btn = sender as Button;
                LotDBEntity.Models.ItemCamImgsAndVideos itemImg = btn.DataContext as LotDBEntity.Models.ItemCamImgsAndVideos;
                MainImg1.DataContext = itemImg;
                LogHelper.Logger.Info("Ends BtnItem_TouchDown_1 in ComparePV Page");
            }
            catch (Exception ex)
            {
                 LogHelper.Logger.ErrorException("Exception: ComparePVPage : BtnItem_TouchDown_1: " + ex.Message, ex);
            }
            
        }

        private void BtnItem_TouchDown_2(object sender, TouchEventArgs e)
        {
            try
            {
                LogHelper.Logger.Info("Starts BtnItem_TouchDown_1 in ComparePV Page");
                Button btn = sender as Button;
                LotDBEntity.Models.ItemCamImgsAndVideos itemImg = btn.DataContext as LotDBEntity.Models.ItemCamImgsAndVideos;
                MainImg2.DataContext = itemImg;
                LogHelper.Logger.Info("Ends BtnItem_TouchDown_1 in ComparePV Page");
            }
            catch (Exception ex)
            {
                LogHelper.Logger.ErrorException("Exception: ComparePVPage : BtnItem_TouchDown_2: " + ex.Message, ex);
            }
        }
    }
}
