﻿#pragma checksum "..\..\UserOptions.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "35D40A881CD7ADEFA7A4B487FB79F9B9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LotsMobile.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LotsMobile {
    
    
    /// <summary>
    /// UserOptions
    /// </summary>
    public partial class UserOptions : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 75 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdImage;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgpg2;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border textHeading;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid searchByFilterPanel;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkPricePanel;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CboPrice;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkBrandPanel;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CboBrand;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSearch;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\UserOptions.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFindUrOffer;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/useroptions.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\UserOptions.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\UserOptions.xaml"
            ((LotsMobile.UserOptions)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.grdImage = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.imgpg2 = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.textHeading = ((System.Windows.Controls.Border)(target));
            return;
            case 5:
            this.searchByFilterPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.stkPricePanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 7:
            this.CboPrice = ((System.Windows.Controls.ComboBox)(target));
            
            #line 172 "..\..\UserOptions.xaml"
            this.CboPrice.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboPrice_SelectionChanged_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.stkBrandPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 9:
            this.CboBrand = ((System.Windows.Controls.ComboBox)(target));
            
            #line 189 "..\..\UserOptions.xaml"
            this.CboBrand.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CboBrand_SelectionChanged_1);
            
            #line default
            #line hidden
            return;
            case 10:
            this.btnSearch = ((System.Windows.Controls.Button)(target));
            
            #line 205 "..\..\UserOptions.xaml"
            this.btnSearch.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSearch_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 11:
            this.btnFindUrOffer = ((System.Windows.Controls.Button)(target));
            
            #line 219 "..\..\UserOptions.xaml"
            this.btnFindUrOffer.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnFindUrOffer_TouchDown_1);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

