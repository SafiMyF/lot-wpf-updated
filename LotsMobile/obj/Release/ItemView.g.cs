﻿#pragma checksum "..\..\ItemView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9A8065A8F12AFF79E9E869D00181EA01"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LotsMobile.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LotsMobile {
    
    
    /// <summary>
    /// ItemView
    /// </summary>
    public partial class ItemView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 120 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdItemView;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollViewerListing;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ListingThumbnails;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkPnlSelectedImg;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkMainInfo;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemName;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock price;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkOS;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemOS;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock e1;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemVersion;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkProcessor;
        
        #line default
        #line hidden
        
        
        #line 197 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkDisplay;
        
        #line default
        #line hidden
        
        
        #line 207 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkCamera;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkMemory;
        
        #line default
        #line hidden
        
        
        #line 260 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stksimilar;
        
        #line default
        #line hidden
        
        
        #line 261 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SimilarPhones;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollViewerSimilarProds;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ListingSimilarProducts;
        
        #line default
        #line hidden
        
        
        #line 276 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock cmpPhone;
        
        #line default
        #line hidden
        
        
        #line 277 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollViewerAccessories;
        
        #line default
        #line hidden
        
        
        #line 280 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ListingAccessories;
        
        #line default
        #line hidden
        
        
        #line 295 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemRating;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUserRating;
        
        #line default
        #line hidden
        
        
        #line 306 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel UserRatingPopup;
        
        #line default
        #line hidden
        
        
        #line 315 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSaveRating;
        
        #line default
        #line hidden
        
        
        #line 326 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnWriteComment;
        
        #line default
        #line hidden
        
        
        #line 332 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer ScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 335 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl UserComments;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCompare;
        
        #line default
        #line hidden
        
        
        #line 354 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCart;
        
        #line default
        #line hidden
        
        
        #line 381 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBanner;
        
        #line default
        #line hidden
        
        
        #line 389 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEMI;
        
        #line default
        #line hidden
        
        
        #line 397 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExchange;
        
        #line default
        #line hidden
        
        
        #line 405 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFinance;
        
        #line default
        #line hidden
        
        
        #line 418 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel OfferBannerPopup;
        
        #line default
        #line hidden
        
        
        #line 423 "..\..\ItemView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtPromotions;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/itemview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ItemView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\ItemView.xaml"
            ((LotsMobile.ItemView)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 5:
            this.grdItemView = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 136 "..\..\ItemView.xaml"
            this.btnClose.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnClose_TouchDown_2);
            
            #line default
            #line hidden
            return;
            case 7:
            this.scrollViewerListing = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 154 "..\..\ItemView.xaml"
            this.scrollViewerListing.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ListingThumbnails = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 9:
            this.stkPnlSelectedImg = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.stkMainInfo = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.itemName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.price = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.stkOS = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 14:
            this.itemOS = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.e1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.itemVersion = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.stkProcessor = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 18:
            this.stkDisplay = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 19:
            this.stkCamera = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 20:
            this.stkMemory = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 21:
            this.stksimilar = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 22:
            this.SimilarPhones = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.scrollViewerSimilarProds = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 264 "..\..\ItemView.xaml"
            this.scrollViewerSimilarProds.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 24:
            this.ListingSimilarProducts = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 25:
            this.cmpPhone = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.scrollViewerAccessories = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 279 "..\..\ItemView.xaml"
            this.scrollViewerAccessories.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 27:
            this.ListingAccessories = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 28:
            this.itemRating = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 29:
            this.btnUserRating = ((System.Windows.Controls.Button)(target));
            
            #line 300 "..\..\ItemView.xaml"
            this.btnUserRating.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnUserRating_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 30:
            this.UserRatingPopup = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 31:
            this.btnSaveRating = ((System.Windows.Controls.Button)(target));
            
            #line 315 "..\..\ItemView.xaml"
            this.btnSaveRating.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSaveRating_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 32:
            this.btnWriteComment = ((System.Windows.Controls.Button)(target));
            
            #line 326 "..\..\ItemView.xaml"
            this.btnWriteComment.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnWriteComment_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 33:
            this.ScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 334 "..\..\ItemView.xaml"
            this.ScrollViewer.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 34:
            this.UserComments = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 35:
            this.btnCompare = ((System.Windows.Controls.Button)(target));
            
            #line 342 "..\..\ItemView.xaml"
            this.btnCompare.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCompare_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 36:
            this.btnCart = ((System.Windows.Controls.Button)(target));
            
            #line 354 "..\..\ItemView.xaml"
            this.btnCart.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCart_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 37:
            this.btnBanner = ((System.Windows.Controls.Button)(target));
            
            #line 381 "..\..\ItemView.xaml"
            this.btnBanner.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBanner_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 38:
            this.btnEMI = ((System.Windows.Controls.Button)(target));
            
            #line 389 "..\..\ItemView.xaml"
            this.btnEMI.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBanner_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 39:
            this.btnExchange = ((System.Windows.Controls.Button)(target));
            
            #line 397 "..\..\ItemView.xaml"
            this.btnExchange.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBanner_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 40:
            this.btnFinance = ((System.Windows.Controls.Button)(target));
            
            #line 405 "..\..\ItemView.xaml"
            this.btnFinance.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBanner_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 41:
            this.OfferBannerPopup = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 42:
            this.txtPromotions = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 28 "..\..\ItemView.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.BtnItem_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 3:
            
            #line 59 "..\..\ItemView.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.BtnSimilarItem_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 4:
            
            #line 90 "..\..\ItemView.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.BtnAccessory_TouchDown_1);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

