﻿#pragma checksum "..\..\ScreenView.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "46535819E0C70A36D622640EB80272FA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LotsMobile.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LotsMobile {
    
    
    /// <summary>
    /// ScreenView
    /// </summary>
    public partial class ScreenView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 144 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdItemView;
        
        #line default
        #line hidden
        
        
        #line 166 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollViewerListing;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ListingThumbnails;
        
        #line default
        #line hidden
        
        
        #line 186 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkPnlSelectedImg;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkMainInfo;
        
        #line default
        #line hidden
        
        
        #line 199 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemName;
        
        #line default
        #line hidden
        
        
        #line 203 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock price;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkOS;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemOS;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemVersion;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkProcessor;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ps1;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkDisplay;
        
        #line default
        #line hidden
        
        
        #line 256 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkCamera;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LabelCamera;
        
        #line default
        #line hidden
        
        
        #line 276 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkMemory;
        
        #line default
        #line hidden
        
        
        #line 285 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LabelMemory;
        
        #line default
        #line hidden
        
        
        #line 331 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock itemRating;
        
        #line default
        #line hidden
        
        
        #line 336 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUserRating;
        
        #line default
        #line hidden
        
        
        #line 342 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel UserRatingPopup;
        
        #line default
        #line hidden
        
        
        #line 351 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSaveRating;
        
        #line default
        #line hidden
        
        
        #line 368 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer ScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 371 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl UserComments;
        
        #line default
        #line hidden
        
        
        #line 391 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBanner;
        
        #line default
        #line hidden
        
        
        #line 399 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEMI;
        
        #line default
        #line hidden
        
        
        #line 407 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExchange;
        
        #line default
        #line hidden
        
        
        #line 415 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnFinance;
        
        #line default
        #line hidden
        
        
        #line 425 "..\..\ScreenView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stkOffer;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/screenview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ScreenView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\ScreenView.xaml"
            ((LotsMobile.ScreenView)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded_1);
            
            #line default
            #line hidden
            
            #line 8 "..\..\ScreenView.xaml"
            ((LotsMobile.ScreenView)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.Window_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.grdItemView = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.scrollViewerListing = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 4:
            this.ListingThumbnails = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 5:
            this.stkPnlSelectedImg = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 6:
            this.stkMainInfo = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 7:
            this.itemName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.price = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.stkOS = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 10:
            this.itemOS = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.itemVersion = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.stkProcessor = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 13:
            this.ps1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.stkDisplay = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 15:
            this.stkCamera = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 16:
            this.LabelCamera = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.stkMemory = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 18:
            this.LabelMemory = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.itemRating = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 20:
            this.btnUserRating = ((System.Windows.Controls.Button)(target));
            return;
            case 21:
            this.UserRatingPopup = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 22:
            this.btnSaveRating = ((System.Windows.Controls.Button)(target));
            return;
            case 23:
            this.ScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 24:
            this.UserComments = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 25:
            this.btnBanner = ((System.Windows.Controls.Button)(target));
            return;
            case 26:
            this.btnEMI = ((System.Windows.Controls.Button)(target));
            return;
            case 27:
            this.btnExchange = ((System.Windows.Controls.Button)(target));
            return;
            case 28:
            this.btnFinance = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.stkOffer = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

