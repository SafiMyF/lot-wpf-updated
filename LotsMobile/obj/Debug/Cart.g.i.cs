﻿#pragma checksum "..\..\Cart.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "CB128AB67D51D17EBE3BDD0627DBF72F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LotsMobile.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LotsMobile {
    
    
    /// <summary>
    /// Cart
    /// </summary>
    public partial class Cart : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 207 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdCartMainPanel;
        
        #line default
        #line hidden
        
        
        #line 228 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSendEmail;
        
        #line default
        #line hidden
        
        
        #line 242 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgEmptyCart;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCartbg;
        
        #line default
        #line hidden
        
        
        #line 245 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer ScrollViewer;
        
        #line default
        #line hidden
        
        
        #line 250 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl CartContext;
        
        #line default
        #line hidden
        
        
        #line 255 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCart;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid sendEmailGrid;
        
        #line default
        #line hidden
        
        
        #line 286 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\Cart.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSendLink;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LotsMobile;component/cart.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Cart.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\Cart.xaml"
            ((LotsMobile.Cart)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded_1);
            
            #line default
            #line hidden
            return;
            case 7:
            this.grdCartMainPanel = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.btnSendEmail = ((System.Windows.Controls.Button)(target));
            
            #line 228 "..\..\Cart.xaml"
            this.btnSendEmail.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSendEmail_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.imgEmptyCart = ((System.Windows.Controls.Image)(target));
            return;
            case 10:
            this.imgCartbg = ((System.Windows.Controls.Image)(target));
            return;
            case 11:
            this.ScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 246 "..\..\Cart.xaml"
            this.ScrollViewer.PreviewTouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchDown_1);
            
            #line default
            #line hidden
            
            #line 247 "..\..\Cart.xaml"
            this.ScrollViewer.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchMove_1);
            
            #line default
            #line hidden
            
            #line 247 "..\..\Cart.xaml"
            this.ScrollViewer.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.ScrollViewer_PreviewTouchUp_1);
            
            #line default
            #line hidden
            
            #line 247 "..\..\Cart.xaml"
            this.ScrollViewer.ScrollChanged += new System.Windows.Controls.ScrollChangedEventHandler(this.ScrollViewer_ScrollChanged_1);
            
            #line default
            #line hidden
            
            #line 249 "..\..\Cart.xaml"
            this.ScrollViewer.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.ScrollViewer_ManipulationBoundaryFeedback_1);
            
            #line default
            #line hidden
            return;
            case 12:
            this.CartContext = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 13:
            this.imgCart = ((System.Windows.Controls.Image)(target));
            return;
            case 14:
            this.sendEmailGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 15:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 286 "..\..\Cart.xaml"
            this.btnClose.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnClose_TouchDown_1);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnSendLink = ((System.Windows.Controls.Button)(target));
            
            #line 300 "..\..\Cart.xaml"
            this.btnSendLink.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnClose_TouchDown_1);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 2:
            
            #line 21 "..\..\Cart.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnRemoveItemAccesory_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 3:
            
            #line 45 "..\..\Cart.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCartItemSelect_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 4:
            
            #line 107 "..\..\Cart.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnPlus_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 5:
            
            #line 121 "..\..\Cart.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnRemoveItem_TouchDown_1);
            
            #line default
            #line hidden
            break;
            case 6:
            
            #line 178 "..\..\Cart.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnRemoveItem_TouchDown_1);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

